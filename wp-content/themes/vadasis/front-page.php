<?php
	/**
	 * Justin Le
	 */

//	code referenced from: http://gregrickaby.com/genesis-code-snippets/
// Remove Page Title
	remove_action( 'genesis_entry_header', 'genesis_do_post_title' );

// Force full width content (optional)
	add_filter( 'genesis_pre_get_option_site_layout', '__genesis_return_full_width_content' );
//	Remove default genesis loop
	remove_action( 'genesis_loop', 'genesis_do_loop' );

	genesis();